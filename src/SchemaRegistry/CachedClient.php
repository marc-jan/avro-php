<?php

/**
 * Copyright 2020 Joyride GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Avro\SchemaRegistry;

use function Amp\call;
use Amp\Promise;
use Doctrine\Common\Cache\Cache;

final class CachedClient implements AsyncClient
{
    private $cache;

    private $decorated;

    public function __construct(
        Cache $cache,
        AsyncClient $decorated
    ) {
        $this->cache = $cache;
        $this->decorated = $decorated;
    }

    public function getRegisteredSchemaId(string $subject, string $schema): Promise
    {
        return call(function () use ($subject, $schema) {
            $key = self::getCacheKey($schema);
            if (false === $id = $this->cache->fetch($key)) {
                $id = yield $this->decorated->getRegisteredSchemaId($subject, $schema);

                if (\is_int($id) && !$this->cache->save($key, $id)) {
                    throw CacheException::persistenceFailed($key, $id);
                }
            }

            return $id;
        });
    }

    public function registerSchema(string $subject, string $schema): Promise
    {
        return call(function () use ($subject, $schema) {
            $key = self::getCacheKey($schema);
            $id = yield $this->decorated->registerSchema($subject, $schema);
            if (\is_int($id) && !$this->cache->save($key, $id)) {
                throw CacheException::persistenceFailed($key, $id);
            }

            return $id;
        });
    }

    public function getSchema(int $id): Promise
    {
        return call(function () use ($id) {
            $key = (string) $id;
            if (false === $schema = $this->cache->fetch($key)) {
                $schema = yield $this->decorated->getSchema($id);
                if (\is_string($schema) && !$this->cache->save($key, $schema)) {
                    throw CacheException::persistenceFailed($key, $schema);
                }
            }

            return $schema;
        });
    }

    private static function getCacheKey(string $data): string
    {
        return \hash('fnv1a64', $data);
    }
}
